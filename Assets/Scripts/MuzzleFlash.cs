﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MuzzleFlash : MonoBehaviour
{
    [SerializeField]
    private Light muzzleLight;

    [SerializeField]
    private int flashIntensity = 1;

    [SerializeField]
    private float flashDuration;

    private float timer = 0;
    private bool isFiring = false;

    // Start is called before the first frame update
    void Start()
    {
        muzzleLight.intensity = 0;
        flashDuration = flashDuration / 1000;
    }

    // Update is called once per frame
    void Update()
    {
        if (isFiring)
        {
            timer += Time.deltaTime;
            muzzleLight.intensity -= Time.deltaTime * (flashIntensity / flashDuration);
        }
        if (timer > flashDuration)
        {
            muzzleLight.intensity = 0;
            isFiring = false;
            timer = 0;
        }
    }

    public void Fire()
    {
        muzzleLight.intensity = flashIntensity;
        isFiring = true;
    }
    
}
